package com.bootcamp.flick_test_android.core.di

import com.bootcamp.flick_test_android.core.data.network.api.DiscoverApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideDiscoverApi(retrofit: Retrofit) : DiscoverApi{
        return retrofit.create(DiscoverApi::class.java)
    }
}