package com.bootcamp.flick_test_android.core.exception

import  com.bootcamp.flick_test_android.core.exception.Failure.FeatureFailure

class NotFoundFailure {

    class DataNotExist() : Failure.FeatureFailure()

    class EmptyListLoadMore() : FeatureFailure()
}
