package com.bootcamp.flick_test_android.view.main.item

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bootcamp.flick_test_android.core.domain.model.Result
import com.bootcamp.flick_test_android.core.ext.loadImage
import com.bootcamp.flick_test_android.databinding.ItemMovieBinding
import com.mikepenz.fastadapter.binding.AbstractBindingItem

class MovieItem(val result : Result) : AbstractBindingItem<ItemMovieBinding>() {
    override val type: Int
        get() = hashCode()

    override fun createBinding(inflater: LayoutInflater, parent: ViewGroup?): ItemMovieBinding {
        return ItemMovieBinding.inflate(inflater,parent,false)
    }

    override fun bindView(binding: ItemMovieBinding, payloads: List<Any>) {
        binding.run {
            tvTitle.text = result.title
            tvOverview.text = result.overview
            tvReleasedate.text = result.releaseDate
            result?.posterPath?.let { imageViews.loadImage(it) }
        }
    }

}