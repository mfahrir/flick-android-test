package com.bootcamp.flick_test_android.core.domain.repository

import com.bootcamp.flick_test_android.core.data.response.DiscoverMovieResponse
import com.bootcamp.flick_test_android.core.domain.model.DiscoverMovie
import io.reactivex.Single
import retrofit2.http.Query

interface DiscoverRepository {

    fun getMovies(apiKey : String,page : Int) : Single<DiscoverMovie>
}