package com.bootcamp.flick_test_android.view.splashscreen

import com.bootcamp.flick_test_android.view.main.MainActivity

class SplashRouter {

    fun openMainScreen(source: SplashActivity) {
        source.startActivity(MainActivity.createIntent(source))
        source.finish()
    }
}