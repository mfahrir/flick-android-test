package com.bootcamp.flick_test_android.view.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.bootcamp.flick_test_android.core.base.BaseActivity
import com.bootcamp.flick_test_android.core.domain.model.Result
import com.bootcamp.flick_test_android.core.ext.loadImage
import com.bootcamp.flick_test_android.databinding.ActivityDetailMovieBinding
import com.bootcamp.flick_test_android.view.main.MainRouter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MovieDetailActivity : BaseActivity<ActivityDetailMovieBinding>(), HasAndroidInjector{
    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private var resultMovie : Result? = null


    override fun enableBackButton(): Boolean  = true

    override fun bindToolbar(): Toolbar? = viewBinding?.movieDetailToolbar

    override fun getUiBinding(): ActivityDetailMovieBinding {
        return ActivityDetailMovieBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?) {
        viewBinding?.run {
            resultMovie = intent.getParcelableExtra(EXTRA_RESULT_MOVIE)
            resultMovie?.let {
                it.backdropPath?.let { it1 -> movieDetailbackdropImageView.loadImage(it1) }
                it.posterPath?.let { it1 -> detailMovieImgPoster.loadImage(it1) }
                movieDetailTitleText.text = it.title
                movieDetailratingTextView.text = it.voteAverage.toString()
                movieDetailReleaseDateTv.text = it.releaseDate
                movieDetailPopularityTextView.text = it.popularity.toString()
                movieDetailPlotTextView.text = it.overview
            }
        }

    }

    override fun initUiListener() {

    }

    override fun androidInjector(): AndroidInjector<Any>  = androidInjector

    companion object {
        private const val EXTRA_RESULT_MOVIE = "MovieDetailActivity.movieResult"
        fun createIntent(caller: Context, resultMovie : Result): Intent {
            return Intent(caller, MovieDetailActivity::class.java)
                .putExtra(EXTRA_RESULT_MOVIE, resultMovie)
        }
    }
}