package com.bootcamp.flick_test_android.core.di

import com.bootcamp.flick_test_android.view.detail.MovieDetailActivity
import com.bootcamp.flick_test_android.view.main.MainActivity
import com.bootcamp.flick_test_android.view.splashscreen.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindMovieDetailActivity(): MovieDetailActivity

    @ContributesAndroidInjector
    abstract fun bindSplashActivity(): SplashActivity



}