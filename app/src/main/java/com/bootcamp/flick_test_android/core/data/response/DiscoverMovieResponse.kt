package com.bootcamp.flick_test_android.core.data.response


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import com.bootcamp.flick_test_android.core.domain.model.DiscoverMovie

@Keep
data class DiscoverMovieResponse(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("results")
    val resultResponses: List<ResultResponse>?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("total_results")
    val totalResults: Int?
){
    fun toDiscoveryMovie() : DiscoverMovie{
        return DiscoverMovie(
            page,
            resultResponses?.map { it.toResult() },
            totalPages,
            totalResults
        )
    }
}