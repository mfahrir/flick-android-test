package com.bootcamp.flick_test_android.core.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bootcamp.flick_test_android.core.di.viewmodel.ViewModelFactory
import com.bootcamp.flick_test_android.core.di.viewmodel.ViewModelKey
import com.bootcamp.flick_test_android.view.main.MainActivityVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityVM::class)
    abstract fun bindDoctorDetailVM(viewModel: MainActivityVM): ViewModel
}