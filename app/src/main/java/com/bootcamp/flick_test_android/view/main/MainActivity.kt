package com.bootcamp.flick_test_android.view.main

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bootcamp.flick_test_android.R
import com.bootcamp.flick_test_android.core.base.BaseActivityVM
import com.bootcamp.flick_test_android.core.domain.model.DiscoverMovie
import com.bootcamp.flick_test_android.core.exception.Failure
import com.bootcamp.flick_test_android.core.exception.NotFoundFailure
import com.bootcamp.flick_test_android.core.ext.observe
import com.bootcamp.flick_test_android.databinding.ActivityMainBinding
import com.bootcamp.flick_test_android.view.common.item.MovieEmptyItem
import com.bootcamp.flick_test_android.view.common.item.ProgressBarItem
import com.bootcamp.flick_test_android.view.main.item.MovieItem
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericFastItemAdapter
import com.mikepenz.fastadapter.adapters.GenericItemAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.scroll.EndlessRecyclerOnScrollListener
import timber.log.Timber

class MainActivity : BaseActivityVM<ActivityMainBinding,MainActivityVM>() {


    private lateinit var itemAdapter   : GenericItemAdapter
    private lateinit var fastAdapter   : GenericFastItemAdapter
    private lateinit var endlessScroll : EndlessRecyclerOnScrollListener

    private val router = MainRouter()

    override fun enableBackButton(): Boolean  = false
    override fun bindToolbar(): Toolbar?  = null

    override fun getUiBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onFirstLaunch(savedInstanceState: Bundle?) {
        baseViewModel?.getMovies(1)
        initRecyclerView()
    }

    override fun initUiListener() {
        viewBinding?.mainSwipe?.setOnRefreshListener {
            baseViewModel?.getMovies(1)
        }

        fastAdapter.onClickListener = { _,_,item,_ ->
            if (item is MovieItem){
                router.goMovieDetail(this@MainActivity,item.result)
            }
            false
        }
    }

    private fun initRecyclerView() {
        viewBinding?.run {
            fastAdapter = FastItemAdapter()
            itemAdapter = ItemAdapter.items()
            fastAdapter.addAdapter(1, itemAdapter)

            endlessScroll = object : EndlessRecyclerOnScrollListener(itemAdapter) {
                override fun onLoadMore(currentPage: Int) {
                    mainRecycleview.post {
                        itemAdapter.clear()
                    }
                    baseViewModel?.loadMore(currentPage + 1)
                }

            }

            mainRecycleview.let {
                it.layoutManager = LinearLayoutManager(this@MainActivity)
                it.adapter = fastAdapter
                it.addOnScrollListener(endlessScroll)
            }
        }
    }

    override fun observeViewModel(viewModel: MainActivityVM) {
        observe(viewModel.failureLiveData, ::handleFailure)
        observe(viewModel.isLoadingLiveData, ::handleLodingInitialValue)
        observe(viewModel.loadingLoadMoreEvent, ::handleLoadingLoadMore)
        observe(viewModel.discoverMovieLoadMoreEvent,:: handleLoadMoreMovie)
        observe(viewModel.discoverMovieEvent, ::handleMovie)
    }

    private fun handleLoadMoreMovie(discoverMovie: DiscoverMovie?) {
        discoverMovie?.let {
            it.resultResponses?.map {
                fastAdapter.add(MovieItem(it))
            }
        }
    }

    private fun handleLodingInitialValue(showLoading : Boolean?) {
        viewBinding?.mainSwipe?.isRefreshing = showLoading == true
    }

    private fun handleMovie(discoverMovie: DiscoverMovie?) {
        discoverMovie?.let {
            Handler(Looper.getMainLooper()).postDelayed({
                fastAdapter.clear()
                it.resultResponses?.map { result -> fastAdapter.add(MovieItem(result)) }
                endlessScroll.run {
                    enable()
                    resetPageCount(1)
                }
            },300)

        }
    }


    private fun handleLoadingLoadMore(showLoading: Boolean?) {
        viewBinding?.mainRecycleview?.post {
            if (showLoading == true && fastAdapter.itemCount > 0) {
                itemAdapter.add(ProgressBarItem())
            } else {
                itemAdapter.clear()
            }
        }
    }


    override fun handleFailure(failure: Failure?) {
        when (failure) {
            is NotFoundFailure.DataNotExist -> {
                fastAdapter.clear()
                fastAdapter.add(MovieEmptyItem())
            }
            is NotFoundFailure.EmptyListLoadMore -> endlessScroll.disable()
            else -> super.handleFailure(failure)
        }
    }


    override fun bindViewModel(): MainActivityVM {
        return ViewModelProvider(this, viewModelFactory)[MainActivityVM::class.java]
    }


    companion object {
        fun createIntent(caller: Context): Intent {
            return Intent(caller, MainActivity::class.java)
        }
    }
}