package com.bootcamp.flick_test_android.core.di

import com.bootcamp.flick_test_android.core.data.network.api.DiscoverApi
import com.bootcamp.flick_test_android.core.data.repositoryimpl.DiscoverRepositoryImpl
import com.bootcamp.flick_test_android.core.domain.repository.DiscoverRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun provideDataRepository(discoverApi: DiscoverApi) : DiscoverRepository{
        return DiscoverRepositoryImpl(discoverApi)
    }
}