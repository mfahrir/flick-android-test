package com.bootcamp.flick_test_android.view.main

import android.content.Context
import com.bootcamp.flick_test_android.core.domain.model.Result
import com.bootcamp.flick_test_android.view.detail.MovieDetailActivity

class MainRouter {
    fun goMovieDetail(context: Context , movieResult : Result){
        context.startActivity(MovieDetailActivity.createIntent(context,movieResult))
    }
}