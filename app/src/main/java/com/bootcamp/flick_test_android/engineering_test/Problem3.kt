package com.bootcamp.flick_test_android.engineering_test

fun main(){
    doFizzBuzzSearch(100)
}

fun doFizzBuzzSearch(number : Int){
    for ( i in 1..number){
        if (i % 3 == 0 && i % 5 == 0){
            println("FizzBuzz")
        }else if (i % 3 == 0){
            println("Fizz")
        }else if (i % 5 == 0){
            println("Buzz")
        }else {
            println(i)
        }
    }
}

