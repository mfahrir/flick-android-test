package com.bootcamp.flick_test_android.core.data.repositoryimpl

import com.bootcamp.flick_test_android.core.data.network.api.DiscoverApi
import com.bootcamp.flick_test_android.core.domain.model.DiscoverMovie
import com.bootcamp.flick_test_android.core.domain.repository.DiscoverRepository
import io.reactivex.Single

class DiscoverRepositoryImpl(private val discoverApi: DiscoverApi) : DiscoverRepository {
    override fun getMovies(apiKey: String,page : Int): Single<DiscoverMovie> {
        return discoverApi.getMovies(apiKey,page).map { it.toDiscoveryMovie() }
    }

}