package com.bootcamp.flick_test_android.view.main

import com.bootcamp.flick_test_android.BuildConfig
import com.bootcamp.flick_test_android.core.base.BaseViewModel
import com.bootcamp.flick_test_android.core.domain.model.DiscoverMovie
import com.bootcamp.flick_test_android.core.domain.usecase.GetDiscoverMovieUseCase
import com.bootcamp.flick_test_android.core.exception.Failure
import com.bootcamp.flick_test_android.core.exception.NotFoundFailure
import com.bootcamp.flick_test_android.core.ext.TagInjection
import com.bootcamp.flick_test_android.core.ext.disposedBy
import com.bootcamp.flick_test_android.core.ext.getGeneralErrorServer
import com.bootcamp.flick_test_android.core.helper.NetworkHandler
import com.bootcamp.flick_test_android.core.helper.SingleLiveEvent
import io.reactivex.Scheduler
import retrofit2.HttpException
import javax.inject.Named

class MainActivityVM  @javax.inject.Inject constructor(
    @Named(TagInjection.UI_Scheduler) uiSchedulers: Scheduler,
    @Named(TagInjection.IO_Scheduler) ioScheduler: Scheduler,
    networkHandler: NetworkHandler,
    private val getDiscoverMovieUseCase: GetDiscoverMovieUseCase
    ) : BaseViewModel(uiSchedulers, ioScheduler, networkHandler) {

        val discoverMovieEvent = SingleLiveEvent<DiscoverMovie>()
        val discoverMovieLoadMoreEvent = SingleLiveEvent<DiscoverMovie>()
        val loadingLoadMoreEvent = SingleLiveEvent<Boolean>()


        fun getMovies(page : Int){
            executeJob {
                getDiscoverMovieUseCase.getMovies(BuildConfig.BASE_API_KEY,page)
                    .compose(applySchedulers())
                    .doOnSubscribe { isLoadingLiveData.value = true }
                    .doOnTerminate { isLoadingLiveData.value = false }
                    .subscribe({
                        discoverMovieEvent.value = it
                    },{
                        handleFailure(it.getGeneralErrorServer())
                        if (it is HttpException && it.code() == 422) {
                            handleFailure(NotFoundFailure.EmptyListLoadMore())
                            return@subscribe
                        }
                        handleFailure(Failure.ServerError(it.message ?: ""))
                    }).disposedBy(disposable)
            }
        }


        fun loadMore(page: Int){
            executeJob {
                getDiscoverMovieUseCase.getMovies(BuildConfig.BASE_API_KEY,page)
                    .compose(applySchedulers())
                    .doOnSubscribe { loadingLoadMoreEvent.value = true }
                    .doOnTerminate { loadingLoadMoreEvent.value = false }
                    .subscribe({
                        discoverMovieLoadMoreEvent.value = it
                    },{
                        handleFailure(it.getGeneralErrorServer())
                        if (it is HttpException && it.code() == 422) {
                            handleFailure(NotFoundFailure.EmptyListLoadMore())
                            return@subscribe
                        }
                        handleFailure(Failure.ServerError(it.message ?: ""))
                    }).disposedBy(disposable)
            }
        }
}