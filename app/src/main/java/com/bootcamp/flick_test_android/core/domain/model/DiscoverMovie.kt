package com.bootcamp.flick_test_android.core.domain.model

import android.os.Parcelable
import com.bootcamp.flick_test_android.core.data.response.ResultResponse
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiscoverMovie (
    val page: Int?,
    val resultResponses: List<Result>?,
    val totalPages: Int?,
    val totalResults: Int?
) : Parcelable