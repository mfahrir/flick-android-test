package com.bootcamp.flick_test_android.core.di

import com.bootcamp.flick_test_android.FlickApps
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class
    ]
)

interface AppComponent : AndroidInjector<FlickApps> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: FlickApps): Builder

        fun build(): AppComponent
    }
}